# Downloader

## Requirements

1. Docker
2. Docker compose plugin

## Setup

Before starting anything, modify the docker-compose to point at all the right places for your configs. Also modify the UID/GID to whatever you are using.

Just run `docker compose up -d` to spin everything up, feel free to cut out anything that you aren't using like Bazarr or Mylar.

## Configuring the vpn

For information on how to setup the vpn, visit [their wiki](https://github.com/qdm12/gluetun-wiki)

## Configuring Prowlarr

Since Prowlarr can push configs directly to the apps instead of configuring them manually, this is the best way to set everything up.

1. Register a new app
2. For the Prowlarr server, put in `http://vpn:9696`
3. For the app server, put in `http://<local ip of your server>:<port>`
4. Put the API key from the app
5. Test

Since Prowlarr is on the vpn networking, you have to use the IP to communicate back to the server since the docker service link is only one direction.

## Configuring qbit

Since all of the networking is through the vpn container, if the vpn goes down then qbit will also lose its networking and will become unusable. For added security, I would recommend setting the bound interface to "tun0" in the qbit webui under the advanced settings.

## Common issues

Sometimes the vpn just doesn't come up or isn't active, you can simply just run a `docker compose up -d` again since the vpn container will be down and it will force everything back online again.